import numpy as np
import matplotlib.pyplot as plt
import concurrent.futures as cf
# from multiprocessing import Pool
from typing import List, Tuple
from functools import partial
from scipy.integrate import odeint


# Souce: https://ths.rwth-aachen.de/research/projects/hypro/lorentz-system/
def lorentz_dynamic(y, t):
    a, b, c = y
    a = float(a)
    b = float(b)
    c = float(c)

    a_dot = 10.0 * (b - a)
    b_dot = a * (28.0 - c) - b
    c_dot = a * b - 8.0 / 3 * c

    dydt = [a_dot, b_dot, c_dot]
    return dydt


def TC_Simulate(Mode, initialCondition, time_bound) -> np.array:
    time_step = 0.003;
    time_bound = float(time_bound)

    number_points = int(np.ceil(time_bound/time_step))
    t = [i*time_step for i in range(0,number_points)]
    if t[-1] != time_step:
        t.append(time_bound)
    trace: np.array = np.zeros((len(t), 4))
    trace[:, 1:] = odeint(lorentz_dynamic, initialCondition, t, hmax=time_step)
    trace[:, 0] = np.array(t)
    return trace

def mapable_parallel_simulator(initial_cond, Mode, time_bound):
    return TC_Simulate(Mode, initial_cond, time_bound)

def TC_Simulate_Batch(Mode: str, initialConditions: np.array, time_bound: float) -> np.array:
    with cf.ProcessPoolExecutor() as p:
        result: List[List[float, float, float, float]] = list(p.map(partial(mapable_parallel_simulator, Mode=Mode, time_bound=time_bound), initialConditions))
    return np.array(result)

if __name__ == "__main__":
    sol = TC_Simulate_Batch("Default", [[15.0, 15.0, 36.0]], 10.0)
    sol = sol[0]
    time = [row[0] for row in sol]
    a = [row[1] for row in sol]
    b = [row[2] for row in sol]
    plt.plot(time, a, "-r")
    plt.plot(time, b, "-g")
    plt.show()
    plt.plot(a, b, "-r")
    plt.show()

