import json
import sys

from src.backend.reachabilityengine import ReachabilityEngine
from src.frontend.scenario import Scenario
from src.frontend.plotter import plot_rtsegment_and_traces
import numpy as np

def main(scenario_file_name):
    np.random.seed(111)
    cur_scenario: Scenario = Scenario(scenario_file_name)
    reachtube_segment: np.array = ReachabilityEngine.get_reachtube_segment(cur_scenario.initial_set)
    plot_rtsegment_and_traces(reachtube_segment, cur_scenario.initial_set.training_traces)



if __name__=='__main__':
    # take json file name from parameter
    main("input/daginput/input_lorentz_test.json")
