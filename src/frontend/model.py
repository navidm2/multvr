import os
import sys
import importlib
from typing import List, Dict, Tuple, Set, Deque
import numpy as np
import copy
from collections import deque, defaultdict

class Model:
    def __init__(self, name: str, variable_names: List[str], directory_path: str, variable_deps=None):
        self.name: str = name
        self.var_names: List['str'] = variable_names
        self.var_inds: Dict['str', int] = {mode_name:ind for ind, mode_name in enumerate(variable_names)}
        self.importModelFunctions(directory_path)
        if variable_deps is not None:
            self.set_variable_dependencies(variable_deps)
        else:
            self.dependent_dims = None
        return

    def importModelFunctions(self, path: str) -> None:
        """
        Load simulation function from given file path
        Note the folder in the examples directory must have __init__.py
        And the simulation function must be named TC_Simulate
        The function should looks like following:
            TC_Simulate(Mode, initialCondition, time_bound)

        Args:
            path (str): Simulator directory.

        Effect:
            adds simulation functions to class

        """
        sys.path.append(os.path.abspath(path))
        mod_name = path.replace('/', '.')
        module = importlib.import_module(mod_name)
        sys.path.pop()
        try:
            self.TC_Simulate_Batch = module.TC_Simulate_Batch
        except AttributeError:
            self.TC_Simulate_Batch = None
        try:
            self.TC_Simulate = module.TC_Simulate
        except AttributeError:
            self.TC_Simulate = None
        return


    def set_variable_dependencies(self, input_dependencies: Dict['str', Dict['str', List['str']]]):
        parsed_dependencies: Dict['str', Dict[int, np.ndarray]] = self.parse_variable_dependencies(input_dependencies)
        self.dependent_dims: Dict['str', Dict[int, np.ndarray]] = Model.compute_indirect_dependencies(parsed_dependencies)
        return


    def parse_variable_dependencies(self, input_dependencies: Dict['str', Dict['str', List['str']]]) -> Dict['str', Dict[int, np.ndarray]]:
        return {mode_name:{dim_name:np.array([self.var_inds[cur_dim_name] for cur_dim_name in input_dependencies[mode_name][dim_name]]) for dim_name in input_dependencies[mode_name]} for mode_name in input_dependencies}



    @staticmethod
    def get_indirect_dependencies(parsed_dependencies: Dict['str', Dict[int, np.ndarray]]) -> Dict['str', Dict[int, np.ndarray]]:
        all_dependencies: Dict['str', Dict[int, np.ndarray]] = {}
        for cur_mode in parsed_dependencies:
            # begin DFS for indirect deps
            cur_dependencies: Dict[int, Set[int]] = {key:set(parsed_dependencies[cur_mode][key]) for key in all_dependencies[cur_mode]}
            for dim_ind in cur_dependencies:
                cur_dependencies[dim_ind].add(dim_ind)
                # initialize BFS
                frontier: Deque[int] = deque((dim_ind,))
                visited: Set[int] = set()
                while len(frontier) > 0:
                    cur_dependent_dim: int = frontier.popleft()
                    cur_dependencies[dim_ind].add(cur_dependent_dim)
                    if cur_dependent_dim not in visited:
                        frontier.extend(parsed_dependencies[cur_mode][cur_dependent_dim])
                        visited.add(cur_dependent_dim)
            all_dependencies[cur_mode] = {key:np.array(list(cur_dependencies[key])) for key in cur_dependencies}
        return all_dependencies


if __name__ == '__main__':
    my_test = Model("lorentz", ["x", "y", "z"], "examples/lorentz")
    assert my_test.TC_Simulate_Batch is not None
